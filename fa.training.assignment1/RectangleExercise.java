import java.util.Scanner;

public class RectangleExercise {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double w = sc.nextDouble();
        double h = sc.nextDouble();
        System.out.println("Area is " + w + " * " + h + " = " + (w*h));
        System.out.println("Perimeter is 2 * (" + w + " + " + h + ") = " + (2*(w+h)));
    }
}
