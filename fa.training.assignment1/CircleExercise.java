import java.util.Scanner;

public class CircleExercise {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double r = sc.nextDouble();
        System.out.println("Perimeter is = " + (double)(2*Math.PI*r));
        System.out.println("Area is = " + (double)(Math.PI*r*r));
    }
}
